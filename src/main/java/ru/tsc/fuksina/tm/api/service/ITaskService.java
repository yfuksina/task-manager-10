package ru.tsc.fuksina.tm.api.service;

import ru.tsc.fuksina.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name);

    List<Task> findAll();

    Task create(String name, String description);

    Task add(Task task);

    void remove(Task task);

    void clear();

}
