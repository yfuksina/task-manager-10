package ru.tsc.fuksina.tm.api.repository;

import ru.tsc.fuksina.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    void remove(Project project);

    void clear();

    List<Project> findAll();

}
