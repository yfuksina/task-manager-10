package ru.tsc.fuksina.tm.repository;

import ru.tsc.fuksina.tm.api.repository.ICommandRepository;
import ru.tsc.fuksina.tm.constant.ArgumentConst;
import ru.tsc.fuksina.tm.constant.TerminalConst;
import ru.tsc.fuksina.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Display developer info"
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Display application version"
    );

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "Display list of terminal commands"
    );

    private static final Command INFO = new Command (
            TerminalConst.INFO, ArgumentConst.INFO, "Show system info"
    );

    private static final Command ARGUMENT = new Command(
            TerminalConst.ARGUMENT, ArgumentConst.ARGUMENT, "Show application arguments"
    );

    private static final Command COMMAND = new Command(
            TerminalConst.COMMAND, ArgumentConst.COMMAND, "Show application commands"
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "Create new task"
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "Show task list"
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "Clear all tasks"
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "Create new project"
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "Show project list"
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Clear all projects"
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null, "Close application"
    );

    private final Command[] terminalCommands = new Command[] {
            ABOUT, VERSION, HELP, INFO,
            ARGUMENT, COMMAND,
            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return terminalCommands;
    }

}
